#!/usr/bin/env python
"""
Bowling
"""

#import sys
#import getopt
import random


class Bowling:
    frames = 10
    bowlerName = ''
    next = ''
    nextnext = ''
    curFrame = 1
    score = list()
    boxscore = list()
    fscore = int()

    def __init__(self):
        pass

    def print_results(self):
        print self.score
        print self.boxscore
        print self.fscore

    def result(self, pins):
        return random.randrange(0, pins)

    def bowler(self, name='Mike'):
        self.bowlerName = name

    def strike(self, frm, score):
        worth = 10
        self.next = ''
        self.nextnext = ''
        if frm < len(score)-1:
            self.next = score[frm+1]
            if frm < len(score)-2 and len(score[frm+1]) == 1:
                self.nextnext = score[frm+2]
        if len(self.next) == 1:
            worth = 20
            if self.nextnext and len(self.nextnext) == 1:
                worth = 30
            else:
                if len(self.nextnext) < 3:
                    worth = worth + self.nextnext[0]
                else:
                    if self.nextnext[0] == 'X':
                        worth = 30
                    else: 
                        worth = worth + self.nextnext[0]
        elif self.next and self.next[1] == '/':
            worth = 20
        else:
            if self.next and frm != 8:
                worth += self.next[0] + self.next[1]
            else:
                if self.next[0] == 'X':
                    worth = 30
                else: 
                    worth = worth + self.next[0]
        return worth

    def spare(self, frm, score):
        worth = 10
        self.next = ''
        if frm < len(score)-1:
            self.next = score[frm+1]
        if len(self.next) == 1:
            worth = 20
        else:
            if self.next and frm != 9:
                if self.next[0] == 'X':
                    worth = 20
                else:
                    worth = worth + self.next[0]
            else:
                if score[frm][1] == '/':
                    if score[frm][2] == 'X':
                        worth = 20
                    else: 
                        worth = worth + score[frm][2]
                elif score[frm][2] == '/':
                    worth = 10
        return worth

    def turn(self, pins=11):
        frame = list()
        turn1 = self.result(pins)
        #turn1 = 10
        if turn1 < 10:
            remains = pins - turn1
            turn2 = self.result(int(remains))
            #turn2 = remains
            if (turn1 + turn2) < 10:
                frame.append(int(turn1))
                frame.append(int(turn2))
            else:
                frame.append(int(turn1))
                frame.append('/')
                if self.curFrame == 10:
                    turn1 = self.result(pins)
                    turn1 = 10
                    if turn1 < 10:
                        frame.append(int(turn1))
                    else:
                        frame.append('X')
        else:
            frame.append('X')
            if self.curFrame == 10:
                turn1 = self.result(pins)
                #turn1 = 10
                if turn1 < 10:
                    remains = pins - turn1
                    turn2 = self.result(int(remains))
                    #turn2 = remains
                    if (turn1 + turn2) < 10:
                        frame.append(int(turn1))
                        frame.append(int(turn2))
                    else:
                        frame.append(int(turn1))
                        frame.append('/')
                else:
                    frame.append('X')
                    turn1 = self.result(pins)
                    #turn1 = 10
                    if turn1 < 10:
                        frame.append(int(turn1))
                    else:
                        frame.append('X')
        print frame
        return frame

    def add_f_score(self, turn):
        self.score.append(turn)

    def tally_score(self, score):
        for f in range(len(score)):
            frm = 0
            tFrame = f
            if len(score[f]) == 1:
                frm = self.strike(f, score)
            else:
                if score[f][1] == '/' and self.curFrame != 9:
                    frm = self.spare(f, score)
                else:
                    if tFrame == 9:
                        if len(score[f]) == 2:
                            frm = score[f][0] + score[f][1]
                        elif len(score[f]) == 3:
                            if score[f][1] == '/':
                                remains = score[f].pop()
                                mini = self.spare(f, score)
                                if remains == 'X':
                                    mini += 10
                                else:
                                    mini += remains
                                frm = mini
                            elif score[f][0] == 'X':
                                frm = 10
                                if score[f][1] == 'X':
                                    frm = 20
                                    if score[f][2] == 'X':
                                        frm = 30
                                elif score[f][1] == '/':
                                    frm = 20
                                else:
                                    frm = frm + score[f][1] + score[f][2]
                        else: 
                            frm = score[f][0]
                    else:
                        frm = score[f][0] + score[f][1]
            self.boxscore.append(frm)
        for s in range(len(self.boxscore)):
            self.fscore += self.boxscore[s]
        return self.fscore

    def game(self):
        f = 1
        while f <= self.frames:
            self.curFrame = f
            self.add_f_score(self.turn())
            f += 1
        self.tally_score(self.score)
        return self.score





if __name__ == "__main__":
    #main(sys.argv[1:]) // for accepting command line arguments
    b = Bowling()
