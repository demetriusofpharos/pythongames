#!/usr/bin/env python
"""
Bowling
"""

from Tkinter import *
from bowling import *

class Bowl_GUI:
    def __init__(self):
        tk = Tk()
        w = self.window(tk)
        #w.pack()
        tk.mainloop()

    def window(self, master):
        b = Bowling()
        frame = Frame(master)
        frame.pack()

        self.qButton = Button(frame, text = "Quit", fg = "red", command = frame.quit)
        self.qButton.pack(side = RIGHT)

        self.bowlButton = Button(frame, text = "Bowl!", fg = "blue", command = b.game())
        self.bowlButton.pack(side = LEFT)

        self.resButton = Button(frame, text = "See Results!", fg = "blue", command = b.print_results())
        self.resButton.pack(side = RIGHT)




if __name__ == "__main__":
    b = Bowl_GUI()
