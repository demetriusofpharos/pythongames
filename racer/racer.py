#!/usr/bin/python
"""
Racer
"""

import sys
#import getopt
#import random
import pprint
p = pprint.PrettyPrinter(indent=4)

from classes.banks.bank import bank
from classes.cars import car as cars
from classes.cars.dodge import dodge
#from classes.cars.dodge import models as d_models
from classes.persons.driver import driver


class racer:
  balance = 0
  b       = ""
  vehicle = ""
  racer   = ""

  def __init__(self):
    self.b = bank()
    self.money()

  def money(self):
    self.balance = self.b.current_balance()

  def buy_car(self, make = 1, model = 1):
    car = dodge()
    car.make_def(make)
    car.model_def(model)
    #self.vehicle = car.make_and_model(d_models)
    #price = self.vehicle['model'][2]
    #self.balance = self.b.withdrawal(price)

  def driver(self, name, profession):
    self.racer = driver()
    self.racer.driver_name(name)
    self.racer.driver_income(profession)
    self.b.deposit(profession['start_cash'])
    return self.racer




def main():
  r = racer()
  name = raw_input("Enter your name: ")
  #profession = raw_input("Choose your profession: ")
  profession = {
                 "title" : "Programmer",
                 "income" : 200,
                 "start_cash" : 1500,
               }
  r.driver(name, profession)
  p.pprint(cars.makes)
  car_select = raw_input("Select a make: ")
  car_select = cars.makes[int(car_select)]
  try:
    d_models = __import__("classes.cars.%s" % car_select.lower())
  except ImportError:
    print "System can't find proper car module"
    sys.exit(2)
    
  p.pprint(vars(d_models))
  make_select = raw_input("Select a model: ")
  #r.buy_car();
  #p.pprint(r.vehicle)
  #p.pprint(vars(r.racer))
  print "Balance: " + str(r.balance)
  print "Program exited."


if __name__ == "__main__":
  #main(sys.argv[1:]) // for accepting command line arguments
  main()
