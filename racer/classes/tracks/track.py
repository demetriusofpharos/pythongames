#!/usr/bin/python
"""
Racer / track
"""

class track(object):
  skill_level = 1
  length      = "1.2 Miles"

  def __init__(self):
    pass

  def length(self, length):
    self.length = length
    return self.length

  def required_skill(self):
    return self.skill_level
