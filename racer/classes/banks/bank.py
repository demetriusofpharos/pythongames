#!/usr/bin/python
"""
Racer / Bank
"""

class bank(object):
  start_money = 0
  balance     = 0
  loan        = False
  payment_due = ""
  loan_amount = 0

  def __init__(self):
    self.balance = self.start_money

  def deposit(self, credit):
    self.balance += credit

  def withdrawal(self, debit):
    self.balance -= debit
    return self.balance

  def loan(self, assets, request = 0):
    if self.loan == True:
      return False
    asset_total = 0
    for (a_name, a_value) in assets.items():
      asset_total += a_value
    asset_total += self.balance
    loan_possible = asset_total * 0.9
    if loan_possible < request:
      self.balance += request
      self.loan_amount = request
      self.payment_due = request * 0.05
      self.loan = True
    return self.loan

  def loan_payment(self, payment):
    if payment < self.payment_due:
      return False
    self.balance -= payment
    self.loan_amount -= payment

  def current_balance(self):
    return self.balance
