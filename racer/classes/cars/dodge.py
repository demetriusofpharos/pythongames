#!/usr/bin/python
"""
Racer / car / dodge
"""

from car import car

models = {
  1  : {
    1 : "Caliber",
    2 : 500,
    3 : "1.8 L",
    4 : "4-speed Manual",
    5 : "Stock Wheels",
    6 : "Stock Tires",
  },
  2  : {
    1 : "Charger",
    2 : 950,
    3 : "2.4 L",
    4 : "5-speed Manual",
    5 : "Stock Wheels",
    6 : "Stock Tires",
  },
}

class dodge(car):

  def __init__(self):
    super(dodge, self).__init__()
