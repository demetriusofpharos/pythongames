#!/usr/bin/python
"""
Racer / car
"""

makes = {
  1  : "Dodge",
  2  : "Toyota",
}


class car(object):
  make    = ""
  model   = ""

  def __init__(self):
    pass

  def make_def(self, make):
    self.make = make
    return self.make

  def model_def(self, model):
    self.model = model
    return self.model

  def make_and_model(self, models):
    own = {
      "make"  : makes[self.make],
      "model" : models[self.model],
    } 
    return own

