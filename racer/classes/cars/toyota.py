#!/usr/bin/python
"""
Racer / car / dodge
"""

from car import car

models = {
  1  : {
    1 : "Celica",
    2 : 650,
    3 : "2.0 L",
    4 : "4-speed Manual",
    5 : "Stock Wheels",
    6 : "Stock Tires",
  },
  1  : {
    1 : "Supra",
    2 : 900,
    3 : "2.2 L",
    4 : "5-speed Manual",
    5 : "Stock Wheels",
    6 : "Stock Tires",
  },
}

class toyota(car):

  def __init__(self):
    super(toyota, self).__init__()

