#!/usr/bin/python
"""
Racer / person / driver
"""

from person import person

class driver(person):
  skill = 0
  name  = ""

  def driver_skill(self, skill):
    self.skill += skill
    return self.skill

  def driver_name(self, name):
    self.name = name
    return self.name

  def driver_income(self, profession):
    self.income = profession['income']
    return self.income
