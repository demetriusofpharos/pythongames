#!/usr/bin/env python
import random
import pprint

pp = pprint.PrettyPrinter(indent=1, width=70, depth=None)

values = {
  "A"  : "A",
  "K"  : "K",
  "Q"  : "Q",
  "J"  : "J",
  "10" : "10",
  "9"  : "9",
  "8"  : "8",
  "7"  : "7",
  "6"  : "6",
  "5"  : "5",
  "4"  : "4",
  "3"  : "3",
  "2"  : "2",
}
deck = {
  "h" : values,
  "c" : values,
  "d" : values,
  "s" : values,
}

class cards:
  card_deck = deck

  def __init__(self):
    pass

  def print_deck(self):
    pp.pprint(self.card_deck);

  def deal_card(self):
    suit = random.choice(self.card_deck.keys())
    card = random.choice(self.card_deck[suit].keys())
    pp.pprint(card + suit)
    


  
  #def rollDie(self, range):
  #  return random.randrange(1, range)

  #def rollDice(self, n, range):
  #  if (n > 1):
  #    r = []
  #    while n > 0:
  #      r.append(self.rollDie(range))
  #      n = n - 1
  #  else:
  #      r = self.rollDie()
  #  return r




def main():
  c = cards()
  c.deal_card()

if __name__ == "__main__":
  #main(sys.argv[1:]) // for accepting command line arguments
  main()
