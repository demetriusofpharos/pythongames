#!/usr/bin/env python
import random

class dice:
    def rollDie(self, range):
        return random.randrange(1, range)

    def rollDice(self, n, range):
        if (n > 1):
            r = []
            while n > 0:
                r.append(self.rollDie(range))
                n = n - 1
        else:
            r = self.rollDie()

        return r
