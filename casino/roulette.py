#!/usr/bin/python
"""
Roulette
"""

import sys
import getopt
import random


class roulette:
    spin = ""
    bet = ""
    betnum = ""
    money = ""
    even = sorted(set(range(2, 38, 2)))
    odd = sorted(set(range(1, 37, 2)))
    zero = set((0, '00'))
    red = set([1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36])
    black = set([2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35])
    wheel = sorted(list(red) + list(black) + list(zero))

    def __init__(self):
        pass

    def game(self, bet, num, totalmoney):
        self.money = totalmoney
        self.spin = self.wheel[self.spin_wheel()]
        if self.place_bet(bet, num):
            print "Spin was: ", self.spin
            print "Your bet was: $", self.bet, " and ", self.betnum
            if self.is_winner(self.spin):
                self.money-= self.bet
                print "You win! You now have: $", self.money
            else:
                self.money-= self.bet
                print "You lost. You now have: $", self.money

    def spin_wheel(self):
        w = len(self.wheel)
        return random.randrange(1, w)

    def place_bet(self, bet, num):
        betted = False
        if bet > self.money:
            print "You do not have that much money. You have: $", self.money
        elif bet <= 0:
            print "You must bet at least $1. You have: $", self.money
        elif num not in self.wheel and num not in ["red", "black", "even", "odd"]:
            print "You must choose a number from the Roulette wheel:"
            print self.wheel, "red", "black", "even", "odd"
        else:
            self.bet = bet
            self.betnum = num
            betted = True
        return betted

    def is_winner(self, spin):
        winner = False
        if spin == self.betnum:
            winner = True
            if spin in self.wheel:
                self.money+= self.bet*35
            elif spin in self.red and self.betnum == "red":
                self.money+= self.bet*2
            elif (spin in self.black and self.bet == "black"):
                self.money+= self.bet*2
            elif spin in self.even and self.betnum == "even":
                self.money+= self.bet*3
            elif spin in self.odd and self.betnum == "odd":
                self.money+= self.bet*3
        return winner







#def main(argv): // for accepting command line arguments
def main():
    quit = False
    money = 1000
    start = money
    win = start*10
    count = 0
    cashout = ""
    while not quit and money > 0 and money < win:
        if count < 1:
            mybet = 1
            mynum = 1
            change = 1
        if count > 0:
            change = raw_input("Would you like to change your bet? ")
        if count < 1 or change in ["yes", "y"]:
            mybet = raw_input("How much would you like to bet? ")
        if mybet not in ["quit","q"]:
            mybet = int(mybet)
            if count < 1 or change in ["yes", "y"]:
                mynum = raw_input("What number would you like to bet on? ")
            if mynum not in ["red", "black", "even", "odd"]:
                mynum = int(mynum)

            count+= 1
            r = roulette()
            r.game(mybet, mynum, money)
            money = r.money
        else:
            quit = True
            print "You have quit."

    if quit or money <= 0 or money > win:
        if money < start:
            cashout = "You lost $%s." % int(start-money )
        else:
            cashout = "You won $%s." % int(money-start)
        counttext = "You played %s times" % count
        print counttext
        print cashout
    else:
        print "Program exited."
    
    """ // for accepting command line arguments
    try:
        opts, args = getopt.getopt(argv, "b:n", ["bet", "num"])
    except getopt.GetoptError:
        print "Don't Panic!"
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-b", "--bet"):
            mybet = arg
        elif opt in ("-n", "--num"):
            mybet = arg
        else:
            print "Don't Panic!"
            sys.exit(2)

    r = roulette(mybet, mynum)
    """


if __name__ == "__main__":
    #main(sys.argv[1:]) // for accepting command line arguments
    main()
