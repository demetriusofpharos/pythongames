#!/usr/bin/python
"""
Roulette
"""

from Tkinter import *
from roulette import *
#import craps

r = roulette()

class Roulette_GUI:
    quitter = False
    frame = ""
    money = 1000
    start = money
    win = start*10
    count = 0
    cashout = ""

    def __init__(self):
        tk = Tk()
        tk.title("Roulette")
        tk.geometry("200x200")
        w = self.window(tk)
        #w.pack()
        tk.mainloop()

    def window(self, master):
        self.frame = Frame(master)

        self.betamount = Entry(self.frame, insertofftime = 600, width = 5)
        #self.betamount.grid(row = 0, sticky = E)
        self.betamount.focus()
        self.betamount.pack(side = LEFT, fill = X, expand = 0)
        self.betnumber = Entry(self.frame, insertofftime = 600, width = 5)
        #self.betamount.grid(row = 0, sticky = W)
        self.betnumber.pack(side = RIGHT, fill = X, expand = 5)
        self.frame.pack()


        self.buttonframe = Frame(master)
        self.betButton = Button(self.buttonframe, text = "Bet!", fg = "blue", bd = 5, relief = GROOVE, command = self.gui_bet)
        self.betButton.pack(side = LEFT)

        self.playButton = Button(self.buttonframe, text = "Play!", fg = "blue", bd = 5, relief = GROOVE, command = self.gui_play)
        self.playButton.pack(side = LEFT)

        self.qButton = Button(self.buttonframe, text = "Quit", fg = "red", bd = 5, relief = GROOVE, command = self.quit_game)
        self.qButton.pack(side = RIGHT)
        self.buttonframe.pack()

    def gui_bet(self):
        r.money = 1000
        self.mybet = self.betamount.get()
        self.mynum = self.betnumber.get()
        if self.mybet not in ["quit","q"]:
           self.mybet = int(self.mybet)
           if self.mynum not in ["red", "black", "even", "odd"]:
               self.mynum = int(self.mynum)

    def gui_play(self):
        if not self.quitter and self.money > self.mybet and self.money < self.win:
            self.count+= 1
            r.game(self.mybet, self.mynum, self.money)
            self.money = r.money

        if self.quitter or self.money < self.mybet or self.money > self.win:
            self.quit_game()
        #else:
            #print "Program exited."
            #self.quit_game()

    def quit_game(self):
        if self.money < self.start:
            self.cashout = "You lost $%s." % int(self.start-self.money )
        else:
            self.cashout = "You won $%s." % int(self.money-self.start)
        self.counttext = "You played %s times" % self.count
        print self.counttext
        print self.cashout
        self.quitter = True
        self.frame.quit()





if __name__ == "__main__":
    r = Roulette_GUI()

