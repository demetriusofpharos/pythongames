#!/usr/bin/env python

#Compute the odds of winning on the first roll
win = 0
win += 6/36.0 # ways to roll 7
win += 2/36.0 # ways to roll 11
print "first roll win =", win

#Compute the odds of losing on the first roll
lose = 0
lose += 1/36.0 # ways to roll 2
lose += 2/36.0 # ways to roll 3
lose += 1/36.0 # ways to roll 12
print "first roll lose =", lose

#Compute the odds of rolling a point (4, 5, 6, 8, 9, 10)
point = 1 # odds must total 1
point -= win # remove odds of winning on first roll
point -= lose # remove odds of loosing on first roll
print "First roll establishes point =", point
