#!/usr/bin/env python
"""
Dictionary

usage: python dictionary.py [options] [file] [word]

options:
    -f [file], --dict [file]        use different dictionary file
    -h, --help                      show this info
    -d, --debug                     turn on debugging
    -l [int], --lang  [int]         (0 for dict lang, 1 for English)
    -w [word], --word [word]        search for specific word
"""

from xml.dom import minidom
import random
#import toolbox
import sys
import getopt


_debug = 0
_lang  = 0

class GetWord:
    """ gets random word """

    def __init__(self, dictionary=None, word=None):
        if dictionary=="" or dictionary==None: dictionary=self.getDefaultDictionary()
        self.loadDictionary(dictionary)
        if word=="" or word==None:
            w = self.randomWord()
            print "\n\n" + w[0] + ":\n\t" + w[1] + "\n"
        else:
            print "\n\nLooking up " + word + ":"
            w = self.lookupWord(word)
            print "\t" + w + "\n"


    def _load(self, dictionary):
        xmldoc = minidom.parse(dictionary)
        #if _debug: print "\n\n" + xmldoc.toxml()
        return xmldoc

    def getDefaultDictionary(self):
        file = "./klingon.xml"
        return file

    def loadDictionary(self, dictionary):
        self.dictionary = self._load(dictionary)
        self.words = {}
        for word in self.dictionary.getElementsByTagName("word"):
            self.words[word.childNodes[1].firstChild.data] = word.childNodes[3].firstChild.data
        #if _debug:
        #    print "\n"
        #    print self.words
        return self.words

    def randomWord(self):
        try:
            chosen = random.choice(self.words.items())
        except IndexError:
            print "IndexError"
        return chosen

    def lookupWord(self, word):
        val = None 
        if _debug:
            print _lang
            print "\n"
            print "LANG"
        if _lang == 0 and self.words.has_key(word):
            val = self.words[word]
        elif _lang == 1:
            for find, search in self.words.items():
                if _debug:
                  print find, search, word
                if search == word:
                    if not val:
                        val = ""
                    val = val + find + "\n\t"
        else:
            if not val:
                val = "The word you searched for - " + word + " - is not in this dictionary"
        return val




def main(argv):
    file = ""
    word = ""
    try:
        opts, args = getopt.getopt(argv, "h:f:w:d:l", ["help", "debug", "word", "dict=", "lang"])
    except getopt.GetoptError:
        print __doc__
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print __doc__
            sys.exit(2)
        elif opt in ("-d", "--debug"):
            global _debug
            _debug = 1
        elif opt in ("-f", "--dict"):
            file = arg
        elif opt in ("-l", "--lang"):
            global _lang
            _lang = 1
        elif opt in ("-w", "--word"):
            word = arg

    d = GetWord(file, word)

if __name__ == "__main__":
    main(sys.argv[1:])
